# Clear Linux* S6-overelay 
[![Docker Repository on Quay](https://quay.io/repository/cogentwebworks/oci-clearlinux-base/status "Docker Repository on Quay")](https://quay.io/repository/cogentwebworks/oci-clearlinux-base)

Clear Linux S6-overelay A minimal container that includes the 's6' process supervisor, to implement a (PID-1) 'init' program that handles the lifecycle of spawned processes.

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

The MIT License
License: MIT

`docker pull quay.io/cogentwebworks/oci-clearlinux-base`

**Running code quality checks:**

To ensure the code quality of this project is kept consistent we make use of pre-commit hooks. To install them, run the commands below.

```bash
brew install pre-commit
pre-commit install --install-hooks -t pre-commit -t commit-msg
```
